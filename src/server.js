import express from 'express';
import morgan from 'morgan';
import socketIO from 'socket.io';
import socketController from './socketController';
import events from './events';

const PORT = 4000;

const app = express();
app.use(express.static('src/static'));
app.use(morgan('dev'));

app.set('view engine', 'pug');
app.set('views', 'src/views');

app.get('/', (req, res) => res.render('home', { events: JSON.stringify(events) }));

const handleListening = () => console.log(`✅ Server running: http://localhost:${PORT}`);

const server = app.listen(PORT, handleListening);

const io = socketIO.listen(server);


io.on('connection', (socket) => socketController(socket, io));

