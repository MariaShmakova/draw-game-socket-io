import events from "./events";
import { chooseWord } from './words';

let sockets = [];
let inProgress = false;
let painter = null;
let word = null;
let timeout = null;

const socketController = (socket, io) => {
    const broadcast = (event, data) => socket.broadcast.emit(event, data);
    const superBroadcast = (event, data) => io.emit(event, data);
    const playerUpdate = () => superBroadcast(events.playerUpdate, { sockets });
    const choosePainter = () => sockets[Math.floor(Math.random() * sockets.length)];
    const startGame = () => {
        if (sockets.length > 1 && !inProgress) {
            inProgress = true;
            painter = choosePainter();
            word = chooseWord();
            superBroadcast(events.gameStarting);
            setTimeout(() => {
                superBroadcast(events.startedGame);
                io.to(painter.id).emit(events.painterNotif, { word });
                timeout = setTimeout(endGame, 30000);
            }, 5000);
        }
    }
    const endGame = () => {
        inProgress = false;
        superBroadcast(events.gameEnded);
        if (timeout !== null) {
            clearTimeout(timeout);
        }
        setTimeout(() => {
            startGame();
        }, 2000);
    }

    const addPoints = (id) => {
        sockets.map(socket => {
            if (socket.id === id) {
                socket.points += 10;
            }
            return socket;
        });
        playerUpdate();
        endGame();
    }

    socket.on(events.setNickname, ({ nickname }) => {
        socket.nickname = nickname;
        sockets.push({ id: socket.id, points: 0, nickname: socket.nickname });
        broadcast(events.newUser, { nickname });
        playerUpdate();
        startGame();
    });

    socket.on(events.disconnect, () => {
        sockets = sockets.filter(aSocket => aSocket.id !== socket.id);
        broadcast(events.disconnected, { nickname: socket.nickname });
        playerUpdate();
        if (sockets.length === 1 || (painter && painter.id === socket.id)) {
            endGame();
        }
    });

    socket.on(events.sendMsg, ({ message }) => {
        if (message === word && socket.id !== painter.id) {
            superBroadcast(events.newMsg, { message: `Угадал ${socket.nickname}! Загаданное слово: ${word}`, nickname: 'Bot' });
            addPoints(socket.id);
        } else {
            broadcast(events.newMsg, { message: message, nickname: socket.nickname });
        }
    });

    socket.on(events.beginPath, ({ x, y }) => {
        broadcast(events.beganPath, { x, y });
    });

    socket.on(events.strokePath, ({ x, y, color }) => {
        broadcast(events.strokedPath, { x, y, color });
    });

    socket.on(events.fill, ({ color }) => {
        broadcast(events.filled, { color });
    });
}

export default socketController;

