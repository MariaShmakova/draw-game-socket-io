const messages = document.getElementById('messages');
import { getSocket } from './sockets';
const form = document.getElementById('sendMessage');

const appendMessage = (text, nickname) => {
    const li = document.createElement('li');
    li.innerHTML = `
        <span class='author ${nickname ? '-incoming' : '-outgoing'}'>${nickname ? nickname : 'You'}:</span> ${text}
    `;
    messages.appendChild(li);
};

export const handleNewMsg = ({ message, nickname }) => {
    appendMessage(message, nickname);
}

const handleAddMessage = (e) => {
    e.preventDefault();
    const input = form.querySelector('input');
    const { value } = input;
    appendMessage(value);
    getSocket().emit(window.events.sendMsg, { message: value });
    input.value = '';
};

if (form) {
    form.addEventListener('submit', handleAddMessage);
}