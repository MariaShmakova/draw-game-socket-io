const body = document.querySelector('body');

const fireNotification = (text, className) => {
    let notice = document.createElement('div');
    notice.innerText = text;
    notice.className = className;
    body.append(notice);
};

export const handleNewUser = ({ nickname }) => {
    fireNotification(`Пользователь ${nickname} только что присоединился`);
};

export const handleDiconnected = ({ nickname }) => {
    fireNotification(`Пользователь ${nickname} только что вышел`);
};