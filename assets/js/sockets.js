let socket = null;
import { handleNewUser, handleDiconnected } from './notifications';
import { handleNewMsg } from './chat';
import { handleBeganPath, handleStrokedPath, handleFilled } from './paint';
import { handleUpdatePlayer, handleStartedGame, handlePainterNotif, handleGameEnded, handleGameStarting } from './players';

export const getSocket = () => socket;

export const initSocket = (aSocket) => {
    const { events } = window;
    socket = aSocket;

    socket.on(events.newUser, handleNewUser)
    socket.on(events.disconnected, handleDiconnected);
    socket.on(events.newMsg, handleNewMsg);
    socket.on(events.beganPath, handleBeganPath);
    socket.on(events.strokedPath, handleStrokedPath);
    socket.on(events.filled, handleFilled);
    socket.on(events.playerUpdate, handleUpdatePlayer);
    socket.on(events.startedGame, handleStartedGame);
    socket.on(events.painterNotif, handlePainterNotif);
    socket.on(events.gameEnded, handleGameEnded);
    socket.on(events.gameStarting, handleGameStarting);
}