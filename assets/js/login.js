
import { initSocket } from './sockets';
const body = document.body;

const LOGGED_OUT = 'loggedOut';
const LOGGED_IN = 'loggedIn';
const NICKNAME = 'nickname';

const nickname = localStorage.getItem(NICKNAME);

const bodyClassName = nickname === null ? LOGGED_OUT : LOGGED_IN;
body.className = bodyClassName;

const logIn = (nickname) => {
    const socket = io('/');
    socket.emit(window.events.setNickname, { nickname });
    initSocket(socket);
}

if (nickname !== null) {
    logIn(nickname);
}

const form = document.querySelector('.loggedOut form');

const formSubmitHandler = (e) => {
    e.preventDefault();
    const input = form.querySelector('input');
    const { value } = input;
    localStorage.setItem(NICKNAME, value);
    input.value = '';
    logIn(value);
    body.className = LOGGED_IN;
}

if (form) {
    form.addEventListener('submit', formSubmitHandler)
}