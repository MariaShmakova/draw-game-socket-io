import { disabledCanvas, hideControls, showControls, enableCanvas, resetCanvas } from "./paint";

const board = document.getElementById('players');
const notif = document.getElementById('jsNotif');

const updatePlayers = (players) => {
    board.innerHTML = '';
    players.forEach(player => {
        const playerElement = document.createElement('span');
        playerElement.innerText = `${player.nickname}: ${player.points}`;
        board.appendChild(playerElement);
    });
}

export const handleUpdatePlayer = ({ sockets }) => updatePlayers(sockets);

const setNotif = (text) => {
    notif.innerText = text;
}

export const handleStartedGame = () => {
    disabledCanvas();
    hideControls();
    setNotif('');
};

export const handlePainterNotif = ({ word }) => {
    enableCanvas();
    showControls();
    setNotif(`Рисуй! Твое слово: ${word}`);
}

export const handleGameEnded = () => {
    setNotif('Игра закончена!');
    disabledCanvas();
    hideControls();
    resetCanvas();
}

export const handleGameStarting = () => setNotif('Игра скоро начнется!');